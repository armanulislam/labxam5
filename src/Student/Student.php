<?php

namespace App\Student;
use App\Message\Message;
use App\Utility\Utility;

class Student{
    public $id="";
    public $Firstname="";
    public $middlename="";
    public $lastname="";
    public $connect;

    public function __construct()
    {
        $this->connect=mysqli_connect("localhost","root","","lab") or die("database connection failed");
    }
    public function prepare($data=""){
        if(array_key_exists("Firstname",$data))
        {
         $this->Firstname=$data['Firstname'] ;  
        }
        if(array_key_exists("middlename",$data))
        {
            $this->middlename=$data['middlename'] ;
        }
        if(array_key_exists("lastname",$data))
        {
            $this->lastname=$data['lastname'] ;
        }
    }
    public function Store(){
        $sql="INSERT INTO `lab`.`studentsname` (`firstname`, `middlename`, `lastname`) 
              VALUES ('".$this->Firstname."','".$this->middlename."','".$this->lastname."')";
        $result= mysqli_multi_query($this->connect,$sql);
    }
    public function index(){
        $allname=array();
        $qurery="SELECT * FROM `studentsname`";
        $result=mysqli_query($this->connect,$qurery);
        while (($row=mysqli_fetch_object($result)))
        {
            $allname[]=$row;
        }
        return $allname;

    }
    public function delete(){
        $sql="DELETE FROM  `lab`.`studentsname` WHERE  `studentsname`.`id` =".$this->id;
        $result=mysqli_query($this->connect,$sql);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

}
