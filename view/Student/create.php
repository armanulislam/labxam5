<!DOCTYPE html>
<html lang="en">
<head>
    <title>Student Informations</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <center><h2>Inline form</h2>
        <form class="form-inline" role="form" method="post" action="store.php">
            <div class="form-group">
                <label for="text">firstname:</label>
                <input type="text" class="form-control" id="text" placeholder="Enter firstname">
            </div>
            <div class="form-group">
                <label for="pwd">middlename:</label>
                <input type="text" class="form-control" id="pwd" placeholder="Enter middlename">
            </div>
            <div class="form-group">
                <label for="lastname">lastname:</label>
                <input type="text" class="form-control" id="lastname" placeholder="Enter lastname">
            </div>

            <button type="submit" class="btn btn-success">Submit</button>
        </form>
</div>

</body>
</html>

