<?php
include_once ('../../vendor/autoload.php');
use App\Student\Student;
use App\Message\Message;
use App\Utility\Utility;
$in=new Student();
$data=$in->index();
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <h2>Student Data</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>  <a href="trashed.php" class="btn btn-primary" role="button">View Trashed list</a>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>serial</th>
                <th>ID</th>
                <th>Firstname</th>
                <th>Middlename</th>
                <th>lastname</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($data as $value){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $value->id?></td>
                <td><?php echo $value->firstname ?></td>
                <td><?php echo $value->middlename ?></td>
                <td><?php echo $value->lastname ?></td>
                <td><a href="view.php?id=<?php echo $value-> id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $value-> id ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $value->id?>" class="btn btn-danger" role="button" id="delete">Delete</a>
                    <a href="trash.php?id=<?php echo $value->id ?>"  class="btn btn-info" role="button">Trash</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    $(document).ready(function(){
        $("#delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
    });

</script>

</body>
</html>
